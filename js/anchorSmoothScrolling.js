$('#main-menu ul li:first a, #sitemap ul li:first a').on('click', function () {
  $('html, body').animate({
    scrollTop: $('#woodstock-revamped').offset().top
  }, 500);
});

$('#main-menu ul li:nth-child(2) a, #sitemap ul li:nth-child(2) a').on('click', function () {
  $('html, body').animate({
    scrollTop: $('#woodstock-mission').offset().top
  }, 500);
});

$('#main-menu ul li:nth-child(3) a, #sitemap ul li:nth-child(3) a').on('click', function () {
  $('html, body').animate({
    scrollTop: $('#woodstock-tickets').offset().top
  }, 500);
});

$('#main-menu ul li:nth-child(4) a, #sitemap ul li:nth-child(4) a').on('click', function () {
  $('html, body').animate({
    scrollTop: $('#woodstock-location').offset().top
  }, 500);
});

$('#main-menu ul li:nth-child(5) a, #sitemap ul li:nth-child(5) a').on('click', function () {
  $('html, body').animate({
    scrollTop: $('#woodstock-lineup').offset().top
  }, 500);
});

$('#main-menu ul li:last a, #sitemap ul li:last a').on('click', function () {
  $('html, body').animate({
    scrollTop: $('#woodstock-contact').offset().top
  }, 500);
});