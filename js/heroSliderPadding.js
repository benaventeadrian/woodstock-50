/* 
  Generar un padding-top en el carrusel hero 
  que sea igual a la altura de la barra de navegación 
*/
function setHeroSliderPadding() {
  document.querySelector('#hero-slider').style.paddingTop = `${document.querySelector('.headroom').offsetHeight}px`;
}

/* 
  Establecer el padding top del carrusel hero tanto cuando
  se carga la página como cuando la misma se redimensiona
*/
window.addEventListener('load', setHeroSliderPadding);
window.addEventListener('resize', setHeroSliderPadding);