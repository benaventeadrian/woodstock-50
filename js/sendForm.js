document.querySelector('.suscribe-form').addEventListener('submit', sendForm);
document.querySelector('.contact-form').addEventListener('submit', sendForm);

function sendForm(e) {  
  var form = e.target,
      action = form.action,
      method = form.method,
      emailValue = null;

  for (var i = 0; i < form.length; i++) {
    if(form[i].classList.contains('type-email')) {
      emailValue = form[i].value;
    }
  }

  if (validateEmail(emailValue)) {
    $.ajax({
      type: method,
      url: action,
      data: $(form).serialize(),
      complete() {
        $(form).hide();

        setTimeout(function() {
          $(form).next().fadeOut();
        }, 2000);

        setTimeout(function() {
          $(form).fadeIn();
        }, 2500);

        $(form).trigger('reset');
      },
      success() {
        $(form).next().html('¡Formulario enviado con éxito!');
      },
      error() {
        $(form).next().html('Ocurrió un error al procesar tu solicitud, por favor reintentalo más tarde');
      }
    });
  }
  e.preventDefault();
}

function validateEmail(emailValue) {
  var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return emailRegex.test(emailValue);
}